import React from "react";
import HeaderCard from "../HeaderCard";

const HeaderContainer = () => {
  const navItems = [
    {
      title: "تخفیف ها و پیشنهادها",
      child: [{ title: "اولین" }, { title: "دومین" }, { title: "سومین" }],
    },
    {
      title: "دسته بندی",
      child: [{ title: "اولین" }, { title: "دومین" }, { title: "سومین" }],
    },
    {
      title: "جدیدترین ها",
      child: [{ title: "اولین" }, { title: "دومین" }, { title: "سومین" }],
    },
  ];

  const renderNavItems = () => {
    return navItems.map((item, index) => {
      return (
        <li key={"navIteme-" + index} className="nav-item dropdown">
          <a
            className="left nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i className="fal fa-bars"></i>
            {item.title}
          </a>
          <div
            className="dropdown-menu text-right"
            aria-labelledby="navbarDropdown"
          >
            {item.child.map((child, index) => {
              return (
                <a
                  key={"navbar-child-" + index}
                  className="dropdown-item"
                  href="#"
                >
                  {child.title}
                </a>
              );
            })}
          </div>
        </li>
      );
    });
  };
  return (
    <div>
      {" "}
      <HeaderCard navItem={renderNavItems()} />
    </div>
  );
};

export default HeaderContainer;
