import React, { useState, useEffect } from "react";
import ShoppingCartContainer from "../../ShoppingCart/ShoppingCartContainer";
import Link from "next/link";

const HeaderCard = ({ data, navItem }) => {
  const [cardClick, setCardClick] = useState(false);

  const onCartClick = () => {
    console.log("im clicked");
    setCardClick(!cardClick);
  };

  //**********************lock and unlock scrolling when shopping cart is open
  function disableScrolling() {
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () {
      window.scrollTo(x, y);
    };
  }

  function enableScrolling() {
    window.onscroll = function () {};
  }

  useEffect(() => {
    if (cardClick) {
      disableScrolling();
    } else {
      enableScrolling();
    }
  }, [cardClick]);
  //*************************************end of scroll lock

  // const { title, sub1, sub2, sub3 } = data;
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light pt-4">
      <Link href="/">
        <a className="navbar-brand">لوگو</a>
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">{navItem}</ul>
        <form className="form-inline my-2 my-lg-0">
          <i className="fal fa-search"></i>
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="جستجو در"
            aria-label="Search"
          />
        </form>
        <ul className="navbar-nav mr-auto user-wrapper">
          <li className="nav-item">
            <a className="nav-link" href="#">
              وارد شوید
            </a>
          </li>
          <li className="nav-link cart" onClick={onCartClick}>
            <a className="nav-link cart-a">
              <i className="fal fa-shopping-cart" />
              <span>1</span>
            </a>
          </li>
        </ul>
      </div>
      <div
        className={`${
          cardClick ? "shopping-cart-bg" : "shopping-cart-bg none"
        }`}
      >
        <div
          className={`shopping-cart-container ${
            cardClick ? "shopping-cart-container open-cart" : ""
          }`}
        >
          <ShoppingCartContainer clicked={onCartClick} />
        </div>
      </div>
    </nav>
  );
};
export default HeaderCard;
