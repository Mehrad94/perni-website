import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import AwesomeScroll from "../../reusableComponent/AwesomeScroll";
import cartActions from "../../store/action/redux/cartActions";

// import Section from "../../../reusableComponent/Section";

const NewestContainer = () => {
  const store = useSelector((state) => {
    return state.cart.cartItems; // we call our reducer in here cart initialState = cartItems, reducer = cart
  });

  console.log({store});

  const dispatch = useDispatch();

  const newestProducts = [
    {
      id: "1",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "2",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "2000 ",
      percent: "%20",
    },
    {
      id: "3",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "4",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "9000 ",
      percent: "%10",
    },
    {
      id: "5",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "7000 ",
      percent: "%30",
    },
    {
      id: "6",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "4000 ",
      percent: "%40",
    },
    {
      id: "7",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "8",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "9",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "10",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "11",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "12",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
  ];
  const svgOrange = "svg-orange";

  const onSelectedProduct = (id, count) => {
    // console.log({ store });
    const product = _.find(store, { id: id });
    // console.log({ product });

    if (product) {
      return dispatch(cartActions.changeCount({ id, count }));
    } else {
      if (count != -1) return dispatch(cartActions.addToCart({ id }));
    }
  };

  const productsList = _.chunk(newestProducts, 2);
  // TODO conncet to databas with saga to get popular data
  return (
    <AwesomeScroll
      onSelectedProduct={onSelectedProduct}
      data={productsList}
      Row={DoubleProductCard}
      svg={svgOrange}
    />
  );
};

export default NewestContainer;
