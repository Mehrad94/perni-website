import React from "react";
import Link from "next/link";

const SliderBannerCard = (props) => {
  const { image, title } = props.info;
  return (
    <figure className="col-lg-3 col-md-5 col-6 px-1">
      <Link href="#">
        <a>
          <img src={image} alt="photo" />
          <figcaption>
            <h5>{title}</h5>
          </figcaption>
        </a>
      </Link>
    </figure>
  );
};
export default SliderBannerCard;
