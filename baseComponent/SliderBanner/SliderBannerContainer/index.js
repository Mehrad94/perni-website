import React from "react";
import SliderBannerCard from "../SliderBannerCard";
import AwesomeScroll from "../../../reusableComponent/AwesomeScroll";

const SliderBannerContainer = () => {
  const bannerCards = [
    { image: "https://picsum.photos/200/100", title: "ماست" },
    { image: "https://picsum.photos/200/100", title: "سر شیر" },
    { image: "https://picsum.photos/200/100", title: "بستنی" },
    { image: "https://picsum.photos/200/100", title: "کره" },
    { image: "https://picsum.photos/200/100", title: "شیر" },
    { image: "https://picsum.photos/200/100", title: "تخم مرغ" },
    { image: "https://picsum.photos/200/100", title: "خامه" },
    { image: "https://picsum.photos/200/100", title: "پنیر" },
    { image: "https://picsum.photos/200/100", title: "دوغ" },
  ];
  // const renderBannerCards = () => {
  //   return bannerCards.map((bannerCard) => (
  //     <SliderBannerCard data={bannerCard} />
  //   ));
  // };
  return (
    <div className="slider-banner-container my-2">
      <AwesomeScroll data={bannerCards} Row={SliderBannerCard} />
    </div>
  );
};
export default SliderBannerContainer;
{
  /* return <div className="banner-container row">{renderBannerCards()}</div>; */
}
