import React from "react";
import { Carousel } from "react-responsive-carousel";
import SliderCard from "../SliderCard";
import SliderArrow from "../SliderArrow";
import SliderArrowP from "../SliderArrowP";

// TODO => we should connect to api and get images
const images = [
  "https://i.picsum.photos/id/213/1366/600.jpg",
  "https://i.picsum.photos/id/243/1366/600.jpg",
  "https://i.picsum.photos/id/223/1366/600.jpg",
  "https://i.picsum.photos/id/253/1366/600.jpg",
];

const SliderContainer = () => {
  return (
    <div className="slider-container row my-5">
      <Carousel
        renderArrowNext={(onClickHandler, hasNext) =>
          hasNext && <SliderArrow onClick={onClickHandler} />
        }
        renderArrowPrev={(onClickHandler, hasPrev) =>
          hasPrev && <SliderArrowP onClick={onClickHandler} />
        }
        swipeable={true}
        transitionTime={1000}
        interval={5000}
        infiniteLoop={true}
        autoPlay={true}
        showThumbs={false}
        showStatus={false}
      >
        {images.map((img) => {
          return <SliderCard image={img} />;
        })}
      </Carousel>
    </div>
  );
};

export default SliderContainer;
