import React from "react";

const SliderCard = (props) => {
  const { image } = props;
  console.log({ props });

  return (
    <div className="slider-card-wrapper">
      <img src={image} alt="1" />
    </div>
  );
};

export default SliderCard;
