import React from "react";
import BannerCard from "../BannerCard";

const BannerContainer = () => {
  const bannerCard = [
    {
      image: "https://picsum.photos/800/360",
      title: "رمضان،ماه پر از برکت",
      desc: "تخفیفات ویژه به مناسبت ماه مبارک رمضان",
    },
  ];
  const renderBannerCard = () =>
    bannerCard.map((card) => <BannerCard data={card} />);

  return <div className="banner-container mb-5">{renderBannerCard()}</div>;
};
export default BannerContainer;
