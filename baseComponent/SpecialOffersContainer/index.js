import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import AwesomeScroll from "../../reusableComponent/AwesomeScroll";
import cartActions from "../../store/action/redux/cartActions";

const SpecialOffersContainer = () => {
  const store = useSelector((state) => {
    return state.cart.cartItems;
  });
  console.log({ bilbilak: store });

  const dispatch = useDispatch();

  const svgBlue = "svg-blue";
  const specialOffersProducts = [
    {
      id: "0awsq1",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq2",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "2000 ",
      percent: "%60",
    },
    {
      id: "0awsq3",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq4",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "9000 ",
      percent: "%60",
    },
    {
      id: "0awsq5",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "7000 ",
      percent: "%60",
    },
    {
      id: "0awsq6",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "4000 ",
      percent: "%60",
    },
    {
      id: "0awsq7",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq8",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq9",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq10",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq11",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "0awsq12",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
  ];

  const onSelectedProduct = (data, count) => {
    const foundedPop = _.find(store, { id: data.id });
    foundedPop
      ? dispatch(cartActions.changeCount({ id: data.id, count }))
      : count != -1 && dispatch(cartActions.addToCart(data));
  };

  const productsList = _.chunk(specialOffersProducts, 2);

  //TODO conncet to databas with saga to get popular data
  return (
    <AwesomeScroll
      onSelectedProduct={onSelectedProduct}
      data={productsList}
      Row={DoubleProductCard}
      svg={svgBlue}
    />
  );
};
export default SpecialOffersContainer;
