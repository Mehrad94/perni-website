import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import AwesomeScroll from "../../reusableComponent/AwesomeScroll";
import cartActions from "../../store/action/redux/cartActions";
// import Section from "../../../reusableComponent/Section";

const DiscountsContainer = () => {
  const store = useSelector((state) => {
    return state.cart.cartItems;
  });

  const dispatch = useDispatch();

  const discountProducts = [
    {
      id: "1awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "2awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "2000 ",
      percent: "%60",
    },
    {
      id: "3awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "4awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "9000 ",
      percent: "%60",
    },
    {
      id: "5awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "7000 ",
      percent: "%60",
    },
    {
      id: "6awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "4000 ",
      percent: "%60",
    },
    {
      id: "7awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "8awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "9awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "10awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "11awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
    {
      id: "12awq",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%60",
    },
  ];
  const svgGreen = "svg-green";

  const onSelectedProduct = (id, count) => {
    const foundedPop = _.find(store, { id: id });
    if (foundedPop) {
      return dispatch(cartActions.changeCount({ id, count }));
    } else {
      if (count != -1) return dispatch(cartActions.addToCart({ id }));
    }
  };

  // //increment
  // const onSelectedProduct = (id, count) => {
  //   const foundedPop = _.find(selectedNewest, ["id", id]);
  //   if (foundedPop) {
  //     if (count === -1 && foundedPop.count === 0) return true;
  //     var newArr = _.map(selectedNewest, function (pop) {
  //       return pop.id === id ? { id: pop.id, count: pop.count + count } : pop;
  //     });
  //     setSelectedNewest(newArr);
  //   } else setSelectedNewest((prev) => [...prev, { id, count }]);
  // };
  const productsList = _.chunk(discountProducts, 2);
  // console.log({ productsList });

  //TODO conncet to databas with saga to get popular data
  return (
    <AwesomeScroll
      onSelectedProduct={onSelectedProduct}
      data={productsList}
      Row={DoubleProductCard}
      svg={svgGreen}
    />
  );
};
export default DiscountsContainer;
