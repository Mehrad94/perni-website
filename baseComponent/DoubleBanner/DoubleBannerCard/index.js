import React from "react";
import Link from "next/link";

const DoubleBannerCard = ({ props }) => {
  const { image, title } = props;
  return (
    <figure className="col-lg-6 col-md-6 col-12 px-3">
      <img src={image} alt="photo" />
      <figcaption>
        <h3>{title}</h3>
        <Link href="#">
          <a>خرید</a>
        </Link>
      </figcaption>
    </figure>
  );
};
export default DoubleBannerCard;
