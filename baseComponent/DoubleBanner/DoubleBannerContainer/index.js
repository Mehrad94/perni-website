import React from "react";
import DoubleBannerCard from "../DoubleBannerCard";

const DoubleBannerContainer = () => {
  const cardData = [
    {
      image: "https://picsum.photos/400/300",
      title: "محصولات جایگزین لبنیات مخصوص گیاهخوار ها",
    },
    {
      image: "https://picsum.photos/400/300",
      title: "محصولات لبنی کاملا ارگانیک",
    },
  ];

  const renderCardData = () => {
    return cardData.map((data) => <DoubleBannerCard props={data} />);
  };
  return (
    <div className="double-banner-container row mx-0 mb-5">
      {renderCardData()}
    </div>
  );
};
export default DoubleBannerContainer;
