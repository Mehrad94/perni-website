import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AwesomeScroll from "../../../reusableComponent/AwesomeScroll";
import _ from "lodash";
import SingleProductCard from "../../../reusableComponent/SingleProductCard";
import cartActions from "../../../store/action/redux/cartActions";
// import Section from "../../../reusableComponent/Section";

const PopularsContainer = () => {
  const populars = [
    {
      id: "01a",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "02b",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "2000 ",
      percent: "%20",
    },
    {
      id: "03c",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "04d",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "9000 ",
      percent: "%10",
    },
    {
      id: "05e",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "7000 ",
      percent: "%30",
    },
    {
      id: "06f",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "4000 ",
      percent: "%40",
    },
    {
      id: "07g",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "08h",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "09i",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "010j",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "011k",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
    {
      id: "012l",
      image: "https://picsum.photos/100/",
      alt: "21",
      category: "ماست",
      productName: "ماست یونانی",
      weight: "200 ",
      price: "3000 ",
      percent: "%30",
    },
  ];

  const store = useSelector((state) => {
    return state.cart.cartItems;
  });

  const dispatch = useDispatch();

  const onSelectedProduct = (id, count) => {
    const foundedPop = _.find(store, { id: id });
    if (foundedPop) {
      return dispatch(cartActions.changeCount({ id, count }));
    } else {
      if (count != -1) return dispatch(cartActions.addToCart({ id }));
    }
  };

  //increment
  // const onSelectedProduct = (id, count) => {
  //   const foundedPop = _.find(store, ["id", id]);
  //   if (foundedPop) {
  //     if (count === -1 && foundedPop.count === 0) return true;
  //     var newArr = _.map(selectedProduct, function (pop) {
  //       return pop.id === id ? { id: pop.id, count: pop.count + count } : pop;
  //     });
  //     setSelectedProduct(newArr);
  //   } else {
  //     if (count === -1) return true;
  //     setSelectedProduct((prev) => [...prev, { id, count }]);
  //   }
  // };

  //TODO conncet to databas with saga to get popular data
  return (
    <AwesomeScroll
      data={populars}
      Row={SingleProductCard}
      onSelectedProduct={onSelectedProduct}
    />
  );
};
export default PopularsContainer;
