import React from "react";
import BannerCard from "../BlogBannerCard";

const BlogBannerContainer = () => {
  const bannerCard = [
    {
      image: "https://picsum.photos/810/360",
      title: "# در_خانه_بمان و با خیال راحت خرید کن !",
      desc:
        "بدون نیاز به خارج شدن از خانه و به خطر انداختن سلامتیت محصولات لبنی مورد نیازت رو تامین کن ",
    },
  ];
  const renderBannerCard = () =>
    bannerCard.map((card) => <BannerCard data={card} />);

  return (
    <div className="blog-banner-container row my-2">{renderBannerCard()}</div>
  );
};
export default BlogBannerContainer;
