import React from "react";
import Link from "next/link";

const BlogBannerCard = ({ data }) => {
  const { image, title, desc } = data;
  return (
    <figure className="col-12">
      <Link href="#">
        <a>
          <img src={image} alt="photo" />
          <div className="title-wrapper">
            <h2>{title}</h2>
            <p>{desc}</p>
            <Link href="#">
              <a>مطالعه بیشتر</a>
            </Link>
          </div>
        </a>
      </Link>
    </figure>
  );
};
export default BlogBannerCard;
