import React from "react";
import Link from "next/link";

const Footer = () => {
  return (
    <footer>
      <div className="row">
        <div className="right-side col-lg-7 offset-1">
          <h2>فروشگاه اینترنتی پرنی</h2>
          <p>
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
            استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در
            ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز،
            و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای
            زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و
            متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان
            رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد
            کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
            راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
            حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود
            طراحی اساسا مورد استفاده قرار گیرد
          </p>
        </div>
        <ul className="left-side col-lg-4">
          <li>
            <Link href="#">
              <a>درباره ما</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>تماس با ما</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>سوالات متداول</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>قوانین و مقررات</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>شماره تماس پشتیبانی: 01333548</a>
            </Link>
          </li>
        </ul>
      </div>
      <h6>ما را در شبکه های اجتماعی دنبال کنید</h6>
      <ul className="social row">
        <li>
          <Link href="#">
            <a>
              <i className="fab fa-instagram"></i>
            </a>
          </Link>
        </li>
        <li>
          <Link href="#">
            <a>
              <i className="fab fa-telegram-plane"></i>
            </a>
          </Link>
        </li>
        <li>
          <Link href="#">
            <a>
              <i className="fab fa-twitter"></i>
            </a>
          </Link>
        </li>
        <li>
          <Link href="#">
            <a>
              <i className="far fa-envelope"></i>
            </a>
          </Link>
        </li>
      </ul>
    </footer>
  );
};
export default Footer;
