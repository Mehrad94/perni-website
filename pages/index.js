import React from "react";
import SliderContainer from "../baseComponent/Slider/SliderContainer";
import PopularsContainer from "../baseComponent/Populars/PopularsContainer";
import Section from "../reusableComponent/Section";
import SliderBannerContainer from "../baseComponent/SliderBanner/SliderBannerContainer";
import DoubleBannerContainer from "../baseComponent/DoubleBanner/DoubleBannerContainer";
import NewestContainer from "../baseComponent/NewestContainer";
import BannerContainer from "../baseComponent/Banner/BannerContainer";
import BlogBannerContainer from "../baseComponent/BlogBanner/BlogBannerContainer";
import DiscountsContainer from "../baseComponent/DiscountsContainer";
import SpecialOffersContainer from "../baseComponent/SpecialOffersContainer";
import Layer from "../reusableComponent/Layers/HomeLayer";

const Home = () => {
  return (
    <div>
      <div className="base-page container-fluid px-0">
        <Layer>
          <SliderContainer />
          <Section
            btnTitle={"مشاهده همه"}
            title={"محبوب ترین ها"}
            Container={PopularsContainer}
          />
          <SliderBannerContainer />
          <Section
            btnTitle={"مشاهده همه"}
            title={"جدیدترین کالاها"}
            Container={NewestContainer}
          />
          <DoubleBannerContainer />
          <Section
            btnTitle={"مشاهده همه"}
            title={"بیشترین تخفیف ها"}
            Container={DiscountsContainer}
          />
          <BannerContainer />
          <Section
            btnTitle={"مشاهده همه"}
            title={"پیشنهاد های ویژه"}
            Container={SpecialOffersContainer}
          />
          <BlogBannerContainer />
        </Layer>
      </div>
    </div>
  );
};

export default Home;
