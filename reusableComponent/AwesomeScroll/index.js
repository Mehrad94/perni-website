import React, { useEffect, useRef } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";

const AwesomeScroll = (props) => {
  const { data, Row, svg } = props;

  const scrollRef = useRef(null);

  useEffect(() => {
    const slider = scrollRef.current.children[0];
    console.log({ slider });

    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      // slider.classList.add("active");
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });

    slider.addEventListener("mouseleave", () => {
      isDown = false;
      // slider.classList.remove("active");
    });

    slider.addEventListener("mouseup", () => {
      isDown = false;
      // slider.classList.remove("active");
    });

    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = x - startX; //scroll-fast

      slider.scrollLeft = scrollLeft - walk;
    });
  }, []);

  return (
    <div>
      <main>
        <div ref={scrollRef} className={svg + " awesome-scroll-container mb-5"}>
          <PerfectScrollbar>
            <ul
              onDragStart={(e) => e.preventDefault()}
              className="awesome-scroll-wrapper"
            >
              {data.map((item, index) => {
                {
                  /* console.log({ awesom: item }); */
                }

                return <Row key={"item-" + index} info={item} {...props} />;
              })}
            </ul>
          </PerfectScrollbar>
        </div>
      </main>
    </div>
  );
};
export default AwesomeScroll;
