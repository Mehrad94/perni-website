import React, { useEffect } from "react";
import _ from "lodash";
import { useSelector } from "react-redux";

const DoubleProductCard = (props) => {
  const { info, onSelectedProduct } = props;

  const store = useSelector((state) => {
    return state;
  });
  const onBuyClick = (inf) => {
    console.log({ FOXINF: inf });
  };
  return (
    <div className="container-column col-lg-2 col-md-4 col-6 px-0 mx-2">
      {info.map((inf, index) => {
        const checkCount = () => {
          const pop = _.find(store.cart.cartItems, ["id", inf.id]);
          return pop ? pop.count : 0;
        };
        return (
          <figure className="single-card-container">
            <span className="percent">{inf.percent}</span>
            <img src={inf.image} alt={inf.alt} />
            <figcaption>
              <h2>{inf.category}</h2>
              <h3>{inf.productName}</h3>
              <h4>
                {inf.weight}
                <span>گرم</span>
              </h4>
              <h2 className="price">
                {inf.price}
                <span>تومان</span>
              </h2>
              <div className="amount">
                <div className="amount-wrapper">
                  <span onClick={() => onSelectedProduct(inf, 1)}>+</span>
                  <span className="num">
                    <p>{checkCount()}</p>
                  </span>
                  <span onClick={() => onSelectedProduct(inf, -1)}>-</span>
                </div>
                <a href="#">
                  <i onClick={() => onBuyClick(inf)} className="fal fa-shopping-bag"></i>
                </a>
              </div>
            </figcaption>
          </figure>
        );
      })}
    </div>
  );
};
export default DoubleProductCard;
