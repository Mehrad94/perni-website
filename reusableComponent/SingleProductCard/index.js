import React from "react";
import { useSelector } from "react-redux";
import _ from "lodash";

const SingleProductCard = (props) => {
  const { info, onSelectedProduct } = props;

  const store = useSelector((state) => {
    return state;
  });

  const {
    id,
    percent,
    alt,
    category,
    productName,
    weight,
    price,
    image,
  } = props.info;

  // const addToCartHandler = () => {
  //   const
  // };

  const checkCount = () => {
    const pop = _.find(store.cart.cartItems, ["id", id]);
    return pop ? pop.count : 0;
  };

  return (
    <figure className="single-card-container col-lg-2 col-md-4 col-6 px-0 mx-2">
      <span className="percent">{percent}</span>
      <img src={image} alt={alt} />
      <figcaption>
        <h2>{category}</h2>
        <h3>{productName}</h3>
        <h4>
          {weight}
          <span>گرم</span>
        </h4>
        <h2 className="price">
          {price}
          <span>تومان</span>
        </h2>
        <div className="amount">
          <div className="amount-wrapper">
            <span onClick={() => onSelectedProduct(id, 1)}>+</span>
            <span className="num">
              <p>{checkCount()}</p>
            </span>
            <span onClick={() => onSelectedProduct(id, -1)}>-</span>
          </div>
          <a onClick={() => addToCartHandler()}>
            <i className="fal fa-shopping-bag"></i>
          </a>
        </div>
      </figcaption>
    </figure>
  );
};
export default SingleProductCard;
