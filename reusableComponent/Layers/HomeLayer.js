import React from "react";
import Footer from "../../baseComponent/Footer/FooterCard";
import HeaderContainer from "../../baseComponent/Header/HeaderContainer";

const Layer = (props) => {
  return (
    <div className="container px-0">
      <HeaderContainer />
      {props.children}
      <Footer />
    </div>
  );
};

export default Layer;
