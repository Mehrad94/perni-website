import React from "react";

const Section = (props) => {
  const { title, btnTitle, Container } = props;

  return (
    <div className="container">
      <div className="section-header-container">
        <h3>{title}</h3>
        <a href="#">{btnTitle}</a>
      </div>
      <Container />
    </div>
  );
};

export default Section;
