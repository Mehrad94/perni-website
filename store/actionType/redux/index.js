const ADD_TO_CART = "ADD_TO_CART_REDUX";
const CHANGE_ITEM_CART_COUNT = "CHANGE_ITEM_CART_COUNT_REDUX";

const atRedux = {
  ADD_TO_CART,
  CHANGE_ITEM_CART_COUNT,
};

export default atRedux;
