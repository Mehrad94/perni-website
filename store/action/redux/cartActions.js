import atRedux from "../../actionType/redux";

const addToCart = (data) => {
  console.log({ DISPATCH: data });
  return {
    type: atRedux.ADD_TO_CART,
    data,
  };
};

const changeCount = (data) => {
  console.log({ data });

  return {
    type: atRedux.CHANGE_ITEM_CART_COUNT,
    data,
  };
};

const cartActions = {
  addToCart,
  changeCount,
};

export default cartActions;
