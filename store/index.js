// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Saga with Redux Implementation &&&&&&&&&&&&&&&&&&&&&&&&&&&

// 0- Prepare redux and saga settings  //next-redux-saga , next-redux-wrapper , react-redux , redux , redux-saga
// 1- add action type for saga                            ||    // actionType/saga
// 2- add action for saga                                 ||    // actions/saga
// 3- add a new action for redux for storing data         ||

//***********for redux */
// 3-1 add a new actionType type for redux                ||   // actionType/redux
//   -- baraye yeki kardan darkhasta
// 3-2 add a new action for redux                         ||   // actions/redux
//   -- baraye inke ye data o begire va reducer makhsoos be uno seda bezane
// 3-3 add a new reducer for redux for updating state     ||   // reducer
//   -- baraye taqeer dadan state

// 4- add a new webService                                ||   // saga/webServices/ GET-POST-PUT-DELETE
// 5- connect webService to redux                         ||   // saga/webServices/
// 6- handle all error to store                           ||   // saga/webServices/
// 7- add watcher to saga                                 ||   // saga/

//********************************** and the end****************************************** */

import { applyMiddleware, createStore } from "redux";
// import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducer";
// import * as watchers from './saga'

//Redux dev tools settings
// const bindMiddleware = (middleware) => {
//   if (process.env.NODE_ENV !== "production") {
//     const { composeWithDevTools } = require("redux-devtools-extension");
//     return composeWithDevTools(applyMiddleware(...middleware));
//   }
//   return applyMiddleware(...middleware);
// }; //for redux devTools

const configureStore = () => {
  const store = createStore(rootReducer, composeWithDevTools());
  return store;
};

export default configureStore;
